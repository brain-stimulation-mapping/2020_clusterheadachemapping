% Heat map computation for cluster headache study, last follow up
% Input: reconstruction and VTAs provided by Schober, Martin
% Processing: n-image using reduction in headache frequency
% Rev 01, Jan/Feb 2020, thuyanhkhoa.nguyen@insel.ch
% Rev 02, Feb/ March 2020, responder and non-responder
% Rev 03, March 2020, n-image, p-image, mean effect and significant mean effect
% Rev 04, April 2020, rethinking mean clinical effect image, not averaging
% by number of centers nor patients per center, but number of activations
% image
% Rev05, June 2020, revisions for manuscript and structural connectivity;
% creating heatmap for responders and non-responders

clearvars
close all

baseFolder = '../../03_Data/VTAs_last_follow_up';
centerFolders = dir( baseFolder );
centerFolders( 1:2 ) = []; % delete . and ..

heatmapAllCenters = ea_load_nii( 'anatT2Resliced.nii' );
mapSizeX = 120; % -15 .. 15
mapSizeY = 180; % -45 .. 0
mapSizeZ = 160; % -20 .. 20
heatmapAllCenters.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
heatmapAllCenters.dim = [ mapSizeX, mapSizeY, mapSizeZ ];
heatmapAllCenters.mat(:,4) = [ -10; -45; -20; 1 ]; % set origin, 20200225 fixed a bug, z was -10 but wanted -20
heatmapResponder = heatmapAllCenters;
heatmapNonResponder = heatmapAllCenters;

nImageNonResponder = heatmapAllCenters;
nImageResponder = heatmapAllCenters;
nImage = heatmapAllCenters;

createVectorReductionHeadacheFrequencyRev03();
numberOfCenters = max( reductionHeadacheFrequencyLastFollowUp(:,2) );
totalCounter = 1;
responderCounter = 0;
nonResponderCounter = 0;

% Rev03
totalVTAs = size( reductionHeadacheFrequencyLastFollowUp, 1 );
singleClinicalScores = NaN( mapSizeX*mapSizeY*mapSizeZ, totalVTAs ); % for p-image
pValueAllCenters = NaN( mapSizeX*mapSizeY*mapSizeZ, 1 ); 
hTestResult = NaN( mapSizeX*mapSizeY*mapSizeZ, 1 ); 

for indexCenter = 1:length( centerFolders )
    VTAfiles = dir( [ baseFolder filesep centerFolders( indexCenter ).name filesep ]);
    VTAfiles( 1:2 ) = [];
    
    for indexVTA = 1:length( VTAfiles )
        fileNameVTA = [ baseFolder filesep centerFolders( indexCenter ).name filesep ...
            VTAfiles( indexVTA ).name];
        reslice_nii( fileNameVTA,...
            'tmpVTAResliced.nii', heatmapAllCenters.voxsize, false )
        VTANifti = ea_load_nii( 'tmpVTAResliced.nii' );
        VTANifti.img(isnan(VTANifti.img)) = 0;
        VTANifti.img = round(VTANifti.img); % clean up
        % 'find' returns linear index, need to convert to subscript
        [xx,yy,zz] = ind2sub(size(VTANifti.img),find(VTANifti.img>0));
        if ~isempty(xx)
            VTAVoxelCoordinates = [xx,yy,zz];
            % map from voxel coordinates to millimeter in MNI world coordinates
            VTAWorldCoordinates = mapVoxelToWorld(VTAVoxelCoordinates,VTANifti);
            if contains(VTAfiles(indexVTA).name,'left')
                VTAWorldCoordinates = ea_flip_lr_nonlinear(VTAWorldCoordinates);
            end
            % map from millimeter world coordinates to voxel coordinates of container
            aggregatedVTAsVoxelCoordinates = mapWorldToVoxel(VTAWorldCoordinates,heatmapAllCenters);
            linearIdx = sub2ind(size(heatmapAllCenters.img),... % get linear index
                aggregatedVTAsVoxelCoordinates(:,1),...
                aggregatedVTAsVoxelCoordinates(:,2),...
                aggregatedVTAsVoxelCoordinates(:,3));

            heatmapAllCenters.img(unique(linearIdx)) = ...
                heatmapAllCenters.img(unique(linearIdx)) + 1*reductionHeadacheFrequencyLastFollowUp(totalCounter,1)/...
                stimulationVoltageLastFollowUp(totalCounter,1);
            singleClinicalScores(unique(linearIdx),totalCounter) = reductionHeadacheFrequencyLastFollowUp(totalCounter,1)/...
                stimulationVoltageLastFollowUp(totalCounter,1);
            nImage.img(unique(linearIdx)) = ...
                    nImage.img(unique(linearIdx)) + 1; 
            if reductionHeadacheFrequencyLastFollowUp(totalCounter,1) > .5 % responder
                nImageResponder.img(unique(linearIdx)) = ...
                    nImageResponder.img(unique(linearIdx)) + 1;
                responderCounter = responderCounter + 1;
                heatmapResponder.img(unique(linearIdx)) = ...
                    heatmapResponder.img(unique(linearIdx)) + 1*reductionHeadacheFrequencyLastFollowUp(totalCounter,1)/...
                    stimulationVoltageLastFollowUp(totalCounter,1);
            else % non-responder, only n-image
                nImageNonResponder.img(unique(linearIdx)) = ...
                    nImageNonResponder.img(unique(linearIdx)) + 1;
                nonResponderCounter = nonResponderCounter + 1;
                heatmapNonResponder.img(unique(linearIdx)) = ...
                    heatmapNonResponder.img(unique(linearIdx)) + 1*reductionHeadacheFrequencyLastFollowUp(totalCounter,1)/...
                    stimulationVoltageLastFollowUp(totalCounter,1);
            end
            totalCounter = totalCounter + 1;
        end
    end
end
totalCounter = totalCounter - 1; % started at 1, correcting here

delete tmpVTAResliced.nii
% discard voxels
voxelsToDiscard = find(nImage.img < 3);
nImage.img(voxelsToDiscard) = 0;
heatmapAllCenters.img(voxelsToDiscard) = 0;
singleClinicalScores(voxelsToDiscard,:) = NaN(numel(voxelsToDiscard),totalVTAs);

% normalization
voxelsToNormalize = find(nImage.img > 0);
heatmapAllCenters.img(voxelsToNormalize) = heatmapAllCenters.img(voxelsToNormalize)./...
    nImage.img(voxelsToNormalize); % calculating mean
% for indexVTA = 1:totalVTAs
%     singleClinicalScores(:,:,:,indexVTA) = singleClinicalScores/numberOfCenters;
% end


%% p-image test
voxelsToTest = find(nImage.img > 2);
for index = 1:numel(voxelsToTest)
    [pValueAllCenters(voxelsToTest(index)), hTestResult(voxelsToTest(index))] = ...
        signrank(singleClinicalScores(voxelsToTest(index),:),...
        0,'alpha',0.001, 'tail','right','method','exact');
end
% clear singleClinicalScores
%% false discovery rate, Genovese et al. (2002)
qFDR = 0.05;
% numberOfVoxels = mapSizeX*mapSizeY*mapSizeZ;
numberOfVoxels = numel(voxelsToTest);
constantV = log(numberOfVoxels) + 0.5772;
pValueBound = ((1:numberOfVoxels)/numberOfVoxels * qFDR/constantV)';
pValueSorted = sort(pValueAllCenters(isfinite(pValueAllCenters)));
rFDR = find(pValueSorted > pValueBound, 1, 'first');
if rFDR > 1
    pValueThreshold = pValueBound(rFDR - 1);
end

%%
% fhAll = plotClusterHeadacheHeatMap(heatmapAllCenters,'All centers, last');
% Saving nifti file
tmpNifti = make_nii(heatmapAllCenters.img, heatmapAllCenters.voxsize, mapWorldToVoxel([0 0 0],heatmapAllCenters));
save_nii(tmpNifti,'heatmapAllCenters20200626.nii');
tmpNifti = make_nii(heatmapResponder.img, heatmapResponder.voxsize, mapWorldToVoxel([0 0 0],heatmapResponder ));
save_nii(tmpNifti,'heatmapResponder20200626.nii');
tmpNifti = make_nii(heatmapNonResponder.img, heatmapNonResponder.voxsize, mapWorldToVoxel([0 0 0],heatmapNonResponder ));
save_nii(tmpNifti,'heatmapNonResponder20200626.nii');
tmpNifti = make_nii(nImage.img, nImage.voxsize, mapWorldToVoxel([0 0 0],nImage));
save_nii(tmpNifti,'nImage20200626.nii');
tmpNifti = make_nii(nImageResponder.img, nImageResponder.voxsize, mapWorldToVoxel([0 0 0],nImageResponder ));
save_nii(tmpNifti,'nImageResponder20200626.nii');
tmpNifti = make_nii(nImageNonResponder.img, nImageNonResponder.voxsize, mapWorldToVoxel([0 0 0],nImageNonResponder ));
save_nii(tmpNifti,'nImageNonResponder20200626.nii');

% save('heatmapPValuesHTest20200626.mat','heatmapAllCenters','pValueAllCenters','hTestResult')
