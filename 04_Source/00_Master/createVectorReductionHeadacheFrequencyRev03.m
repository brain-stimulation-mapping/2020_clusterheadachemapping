% Create vector with reduction in headache frequency [0 1]
% Follow order from folders, i.e., Hannover, Nice, Oxford, Spain new
% Values pulled from Martin Schober Excel files
% Rev01, Jan 2020, thuyanhkhoa.nguyen@insel.ch

reductionHeadacheFrequencyLastFollowUp(:,1) = [...
    .8 .8 .8... % Hannover
    .63 0 .93 1 1 .71 .8 .7 1 .48 .6 .07 .57... % Nice
    .88 .88 1 1 1 1 1 .95... % Oxford
    .71 .52 .97 .75 1 .95 1 .98 .96... % Spain
    1 .43 1 .825 .2 1]'; % Spain

% Adding a center ID, 1 -- Hannover, 2 -- Nice, 3 -- Oxford, 4 -- Spain
reductionHeadacheFrequencyLastFollowUp (:,2) = [...
    1 1 1 ... % Hannover
    repmat(2,1,13) ... % Nice
    repmat(3,1,8) ... % Oxford
    repmat(4,1,15)]'; % Spain

reductionHeadacheFrequency12MonthFollowUp(:,1) = [...
    1 1 1 ... % Hannover
    .63 0 .93 1 1 .71 .8 .7 1 .48 .6 0 .57 ... % Nice
    .95 .95 1 1 1 .89 1 ... % Oxford
    .6 .52 .97 .33 .98 .17 .98 .86 .96 ... % Spain
    .96 .4 .97 .75 .2 .95]'; % Spain

% Adding a center ID, 1 -- Hannover, 2 -- Nice, 3 -- Oxford, 4 -- Spain
reductionHeadacheFrequency12MonthFollowUp (:,2) = [...
    1 1 1 ... % Hannover
    repmat(2,1,13) ... % Nice
    repmat(3,1,7) ... % Oxford
    repmat(4,1,15)]'; % Spain

% Rev 03, adding stimulation voltages for mean effect image
stimulationVoltageLastFollowUp(:,1) = [...
    4 2.5 1.5... % Hannover
    2 1.8 2 2.8 3 3 1 .9 2.3 2  3 1.6 .7... % Nice
    1.8 3.6 2 2 2 2.5 6 1.5... % Oxford
    2.5 2.5 3 2.3 3 1.5 2.5 1.5 1.9... % Spain 1.1 through 1.9
    3.5 4.5 2.3 3.5 3.5 1.5];  % Spain 2 through 2.4

% uncomment this line to have no normalization
% stimulationVoltageLastFollowUp(:,1) = ones(size(reductionHeadacheFrequencyLastFollowUp,1),1);
stimulationVoltageLastFollowUp(:,2) = reductionHeadacheFrequencyLastFollowUp(:,2);
