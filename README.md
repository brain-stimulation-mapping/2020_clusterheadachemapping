# 2020_ClusterHeadacheMapping

Nowacki et al. (2020), Deep Brain Stimulation for Chronic Cluster Headache: Meta-Analysis of Individual Patient Data, https://doi.org/10.1002/ana.25887 .

This repo has the analysis scripts only but not the Nifti files with the volumes of tissue activated. Please contact the main author, if you would like to have this. The analysis scripts are the final version used for the paper and the repo does not include the earlier revisions.

This cluster headache atlas is also available in Lead-DBS.
