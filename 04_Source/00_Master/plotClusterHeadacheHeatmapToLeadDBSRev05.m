% Open Lead DBS, render no patient in 3D with ClusterHeadacheAtlas 2, then
% posterior view, change x-axis slice, run this script to get point cloud
% Rev03, March 2020, responder and non-responder n-images
% Rev04, April 2020
% Rev05, June 2020, revisions for manuscript

hold on
% heatmap = ea_load_nii('Rev03/nImageResponder.nii');
% heatmap = ea_load_nii('Rev03/nImageNonResponder.nii');
% heatmap = ea_load_nii('Rev03/heatmapAllCenters.nii');
heatmap = ea_load_nii('Rev04/heatmapAllCenters20200406.nii');
% heatmap = ea_load_nii('Rev05/heatmapAllCenters20200626.nii');
nImage = ea_load_nii('Rev04/nImage20200406.nii');

%% for responder
tmpIdx = find(heatmap.img > 0);
lowerThreshold = prctile(heatmap.img(tmpIdx),1);
upperThreshold = prctile(heatmap.img(tmpIdx),100);
tmpIdx = find(heatmap.img > lowerThreshold & heatmap.img < upperThreshold);
downSampleFactor = 1;
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(heatmap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatmap);
    
    markerSize = 10;
    scatterColor = [heatmap.img(tmpIdx(1:downSampleFactor:end)) zeros(numel(tmpIdx(1:downSampleFactor:end)),2)];
    scatterColor(:,1) = scatterColor(:,1)/max(heatmap.img(:));
    hMeanClinicalEffect = scatter3(heatMapWorldCoordinates(1:downSampleFactor:end,1),...
        heatMapWorldCoordinates(1:downSampleFactor:end,2),...
        heatMapWorldCoordinates(1:downSampleFactor:end,3),...
        markerSize,scatterColor,'filled');
    hMeanClinicalEffect.MarkerFaceAlpha = 0.3;
    
end
%%
tmpIdx = find(heatmap.img >= upperThreshold);
downSampleFactor = 1;
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(heatmap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatmap);
    
    markerSize = 10;
    % plotting responder in red REVISIT to be rediscussed
    scatterColor = [heatmap.img(tmpIdx(1:downSampleFactor:end)) zeros(numel(tmpIdx(1:downSampleFactor:end)),2)];
    scatterColor(:,1) = scatterColor(:,1)/max(heatmap.img(:));
    hHigh = scatter3(heatMapWorldCoordinates(1:downSampleFactor:end,1),...
        heatMapWorldCoordinates(1:downSampleFactor:end,2),...
        heatMapWorldCoordinates(1:downSampleFactor:end,3),...
        markerSize,scatterColor,'filled');
    hHigh.MarkerFaceAlpha = 0.6;
    
end

%% April 6 mean clinical effect image in parula
% Rev05, July 2020, plotting top ten percent and save as seed
tmpIdx = find(heatmap.img > 0);
lowerThreshold = prctile( heatmap.img( tmpIdx ), 10 );
lowerThreshold90 = prctile( heatmap.img( tmpIdx ), 90 );
upperThreshold = prctile( heatmap.img( tmpIdx ), 100 );
tmpIdx = find(heatmap.img > lowerThreshold & heatmap.img < upperThreshold);
% downSampleFactor = 1;
uniqueMeanClinicalEffectValues = unique( heatmap.img( tmpIdx ) );

% June revisions, using autumn color map and go from red to orange to yellow
% cmap = jet(numel(uniqueMeanClinicalEffectValues));
cmap = autumn(numel( uniqueMeanClinicalEffectValues ));
cmap = flipud( cmap ); % inverting colors from yellow/ low to red/ high
colorTable = zeros( numel( tmpIdx ),3 );
for index = 1:numel( tmpIdx )
    colorTable( index,: ) = cmap( uniqueMeanClinicalEffectValues == ...
        heatmap.img( tmpIdx( index ) ),: );
end
if ~isempty(tmpIdx)
    hold on
    [xx,yy,zz] = ind2sub(size(heatmap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatmap);
    
    markerSize = 20;
%     scatterColor = [zeros(numel(tmpIdx),1) heatmap.img(tmpIdx) zeros(numel(tmpIdx),1)];
%     scatterColor(:,2) = scatterColor(:,2)/max(heatmap.img(:));

    hMeanClinicalEffect = scatter3(heatMapWorldCoordinates(:,1),...
        heatMapWorldCoordinates(:,2),...
        heatMapWorldCoordinates(:,3),...
        markerSize,colorTable,'filled'); 
    hMeanClinicalEffect.MarkerFaceAlpha = 0.3;
    
end


% colorbar
% colormap( cmap )
% cb = colorbar;
% caxis( [ 0 max( heatmap.img( : ) ) ] )
% cb.Label.String = 'Mean clinical efficacy';

%% Rev05, July 2020, top ten percent and save as seed

indexTopTen = find(heatmap.img > lowerThreshold90 & heatmap.img < upperThreshold); 
[ ~, locb ] = ismember( indexTopTen, tmpIdx );
if ~isempty( indexTopTen )
    hold on
    [xx,yy,zz] = ind2sub( size( heatmap.img ), indexTopTen );
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld( heatMapVoxelCoordinates, heatmap) ;
    markerSize = 20;
    hMeanClinicalEffect = scatter3(heatMapWorldCoordinates(:,1),...
        heatMapWorldCoordinates(:,2),...
        heatMapWorldCoordinates(:,3),...
        markerSize,colorTable( locb, : ),'filled'); 
    hMeanClinicalEffect.MarkerFaceAlpha = 0.3;
end

% change views and save within Lead DBS 3D viewer

%% Section
% July 2020, autumn color map
tmpIdx = find(heatmap.img > 0);
lowerThreshold = prctile(heatmap.img(tmpIdx),1);
upperThreshold = prctile(heatmap.img(tmpIdx),100);
tmpIdx = find(heatmap.img > lowerThreshold & heatmap.img < upperThreshold);
% downSampleFactor = 1;
uniqueMeanClinicalEffectValues = unique(heatmap.img(tmpIdx));
% cmap = jet(numel(uniqueMeanClinicalEffectValues));
cmap = autumn(numel( uniqueMeanClinicalEffectValues ));
cmap = flipud( cmap ); % inverting colors from yellow/ low to red/ high
colorTable = zeros(numel(tmpIdx),3);
for index = 1:numel(tmpIdx)
    colorTable(index,:) = cmap(uniqueMeanClinicalEffectValues == ...
        heatmap.img(tmpIdx(index)),:);
end
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(heatmap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatmap);
    
    sectionIndex = find(heatMapWorldCoordinates(:,2) == -15.5);
    
    markerSize = 40;
%     scatterColor = [zeros(numel(tmpIdx),1) heatmap.img(tmpIdx) zeros(numel(tmpIdx),1)];
%     scatterColor(:,2) = scatterColor(:,2)/max(heatmap.img(:));

    hMeanClinicalEffect = scatter3(heatMapWorldCoordinates(sectionIndex,1),...
        heatMapWorldCoordinates(sectionIndex,2),...
        heatMapWorldCoordinates(sectionIndex,3),...
        markerSize,colorTable(sectionIndex,:),'filled'); 
    hMeanClinicalEffect.MarkerFaceAlpha = 1;
    
end

%% nImage
tmpIdx = find(nImage.img > 0);
cmap = jet(numel(unique(nImage.img(tmpIdx))));
colorTable = zeros(numel(tmpIdx),3);
for index = 1:numel(tmpIdx)
    colorTable(index,:) = cmap(nImage.img(tmpIdx(index)) - 2,:);
end
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(nImage.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,nImage);
    markerSize = 20;
    hnImage = scatter3(heatMapWorldCoordinates(:,1),...
        heatMapWorldCoordinates(:,2),...
        heatMapWorldCoordinates(:,3),...
        markerSize,colorTable,'filled'); 
    hnImage.MarkerFaceAlpha = 0.3; 
end

%% for non-responder
heatmap = ea_load_nii('Rev03/nImageNonResponder.nii');
lowerThreshold = 0; %prctile(heatmap.img(tmpIdx),25);
tmpIdx = find(heatmap.img > lowerThreshold);
downSampleFactor = 1;
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(heatmap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatmap);
    
    markerSize = 10;
    % plotting non-responder in dark gray
%     scatterColor = [zeros(numel(tmpIdx(1:downSampleFactor:end)),1) heatmap.img(tmpIdx(1:downSampleFactor:end)) zeros(numel(tmpIdx(1:downSampleFactor:end)),1)];
%     scatterColor(:,1) = scatterColor(:,1)/max(heatmap.img(:));
    scatterColor = .25*ones(numel(tmpIdx(1:downSampleFactor:end)),3);
    hMeanClinicalEffect = scatter3(heatMapWorldCoordinates(1:downSampleFactor:end,1),...
        heatMapWorldCoordinates(1:downSampleFactor:end,2),...
        heatMapWorldCoordinates(1:downSampleFactor:end,3),...
        markerSize,scatterColor,'filled');
    hMeanClinicalEffect.MarkerFaceAlpha = 0.3;
    
end

% change views and save within Lead DBS 3D viewer

%% May et al. (2000)
talCoordinatesInBout = [...
    -2 -18 -8;... % L hypothalamus
    -24 42 12;... % L frontal lobe
    -40 12 -8;... % L insula
    -20 -12 2;... % L basal ganglia
    -60 20 2;... % L sensorimotor area
    2 22 24;... % R anterior cingulate cortex
    4 12 28;... % R mid cingulate cortex
    32 10 2;... % R insula
    6 -12 6;... % R thalamus
    48 30 -20]; % R inferior frontal cortex

talCoordinatesOutBout = [...
    -20 56 6;... % L frontal lobe
    -28 20 -6;... % L insula
    -12 14 8;... % L basal ganglia
    -52 -22 6;... % L temporal lobe
    4 36 12;... % R anterior cingulate cortex
    -2 12 22;... % R mid cingulate cortex
    32 -10 10;... % R insula
    10 -8 4;... % R thalamus
    18 60 10]; % R frontal lobe

MNICoordinatesInBout = tal2icbm_spm(talCoordinatesInBout);
MNICoordinatesOutBout = tal2icbm_spm(talCoordinatesOutBout);

MNICoordinatesInBoutFlipped = MNICoordinatesInBout;
MNICoordinatesInBoutFlipped(1:5,:) = ea_flip_lr_nonlinear(MNICoordinatesInBout(1:5,:));
MNICoordinatesOutBoutFlipped = MNICoordinatesOutBout;
MNICoordinatesOutBoutFlipped(1:4,:) = ea_flip_lr_nonlinear(MNICoordinatesOutBout(1:4,:));

% MayInBout = plot3(MNICoordinatesInBoutFlipped(:,1),MNICoordinatesInBoutFlipped(:,2),MNICoordinatesInBoutFlipped(:,3),...
%     'o','markersize',15,'markerfacecolor',[0 1 0],'markeredgecolor',[0 1 0]);
MayInBout = plot3(MNICoordinatesInBoutFlipped(1,1),MNICoordinatesInBoutFlipped(1,2),MNICoordinatesInBoutFlipped(1,3),...
    'o','markersize',15,'markerfacecolor',[0 1 0],'markeredgecolor',[0 1 0]);
% MayOutBout = plot3(MNICoordinatesOutBoutFlipped(:,1),MNICoordinatesOutBoutFlipped(:,2),MNICoordinatesOutBoutFlipped(:,3),...
%     'o','markersize',15,'markerfacecolor',[0 .7 0],'markeredgecolor',[0 .7 0]);


%% Akram et al. (2017)
MNIAkramLeft = [-6 -13 -6];
MNIAkramRight = ea_flip_lr_nonlinear(MNIAkramLeft);
plot3(MNIAkramRight(1),MNIAkramRight(2), MNIAkramRight(3),'ko','markersize',30,'markerfacecolor','k')

%% Flipping Niftis of non-responders
% four non-responders (reduction of headache frequency less than 50%)
% Nice ; Spain 

VTAFileNames = {... % removed patient intials for Gitlab
    '../../03_Data/VTAs_last_follow_up/VTAs_lastfollowup_nice/xxxx_left.nii';...
    '../../03_Data/VTAs_last_follow_up/VTAs_lastfollowup_nice/xxxx_left_12months.nii';...
    };

for i=1:numel(VTAFileNames)
    VTANifti = ea_load_nii(VTAFileNames{i});
    VTANifti.img(isnan(VTANifti.img)) = 0;
    VTANifti.img = round(VTANifti.img); % clean up
    VTANiftiFlipped = VTANifti;
    VTANiftiFlipped.img = zeros(size(VTANifti.img));
    VTANiftiFlipped.mat(1:3,4) = ea_flip_lr_nonlinear([VTANifti.mat(1,4)+VTANifti.voxsize(1)*VTANifti.dim(1)...
        VTANifti.mat(2:3,4)']);
    
    % 'find' returns linear index, need to convert to subscript
    [xx,yy,zz] = ind2sub(size(VTANifti.img),find(VTANifti.img>0));
    if ~isempty(xx)
        VTAVoxelCoordinates = [xx,yy,zz];
        % map from voxel coordinates to millimeter in MNI world coordinates
        VTAWorldCoordinates = mapVoxelToWorld(VTAVoxelCoordinates,VTANifti);
        VTAWorldCoordinatesFlipped = ea_flip_lr_nonlinear(VTAWorldCoordinates);
        VTAVoxelCoordinatesFlipped = mapWorldToVoxel(VTAWorldCoordinatesFlipped,VTANiftiFlipped);
        linearIdx = sub2ind(size(VTANiftiFlipped.img),... % get linear index
            VTAVoxelCoordinatesFlipped(:,1),...
            VTAVoxelCoordinatesFlipped(:,2),...
            VTAVoxelCoordinatesFlipped(:,3));
        VTANiftiFlipped.img(unique(linearIdx)) = 1;
        tmpNifti = make_nii(VTANiftiFlipped.img, VTANiftiFlipped.voxsize, ...
            mapWorldToVoxel([0 0 0],VTANiftiFlipped));
        save_nii(tmpNifti,['flippedVTA0' int2str(i) '.nii']);
    end
end

%% Sweet spot
% run thresholds above to get the tmpIdx you want
sweetSpot = heatmapAllCenters;
sweetSpot.img(:) = 0;
sweetSpot.img(tmpIdx) = 1;
tmpNifti = make_nii(sweetSpot.img, sweetSpot.voxsize, mapWorldToVoxel([0 0 0],sweetSpot));
save_nii(tmpNifti,'sweetSpotAlpha0x001.nii');

%% plotting significant voxles
tmpIdx = find(hTestResult == 1);
downSampleFactor = 1;
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(heatmap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatmap);
    markerSize = 10;
    % plotting responder in red REVISIT to be rediscussed
    scatterColor = [heatmap.img(tmpIdx(1:downSampleFactor:end)) zeros(numel(tmpIdx(1:downSampleFactor:end)),2)];
    scatterColor(:,1) = scatterColor(:,1)/max(heatmap.img(:));
    hHigh = scatter3(heatMapWorldCoordinates(1:downSampleFactor:end,1),...
        heatMapWorldCoordinates(1:downSampleFactor:end,2),...
        heatMapWorldCoordinates(1:downSampleFactor:end,3),...
        markerSize,scatterColor,'filled');
    hHigh.MarkerFaceAlpha = 0.6;
end

meanImageCoordinates = mean(heatMapWorldCoordinates);
% min(heatMapWorldCoordinates);
% max(heatMapWorldCoordinates);
scatter3(meanImageCoordinates(1), meanImageCoordinates(2), meanImageCoordinates(3), ...
    100,[0 1 0],'filled')
