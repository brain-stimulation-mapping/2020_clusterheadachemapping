% Cluster headache study bootstrapping
% Purpose: perform bootstrapping to test for correlation/ fixed effect
% Method: create effect map with all VTAs, for each patient subtract
% patient's effect and calculate overlap
% Rev01, June 2020, for Annals of Neurology revisions

% clearvars
% close all

baseFolder = '2003_ClusterHeadacheMapping/03_Data/VTAsBootstrap';
% in VTAsBootstrap removed AMM left and 2.2 right, also adapted createVector
centerFolders = dir( baseFolder );
centerFolders(1:2) = []; % delete . and ..

heatmapAllCenters = ea_load_nii('anatT2Resliced.nii');
mapSizeX = 120; % -15 .. 15
mapSizeY = 180; % -45 .. 0
mapSizeZ = 160; % -20 .. 20
heatmapAllCenters.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
heatmapAllCenters.dim = [ mapSizeX, mapSizeY, mapSizeZ ];
heatmapAllCenters.mat(:,4) = [-10; -45; -20; 1]; 

nImageNonResponder = heatmapAllCenters;
nImageResponder = heatmapAllCenters;
nImage = heatmapAllCenters;

createVectorReductionHeadacheFrequencyBootstrapRev01();
numberOfCenters = max(reductionHeadacheFrequencyLastFollowUp( :,2 ));
totalCounter = 0;
responderCounter = 0;
nonResponderCounter = 0;

totalVTAs = size( reductionHeadacheFrequencyLastFollowUp, 1 );

%% Build full map with all patients
tic
for indexCenter = 1:numel(centerFolders)
    VTAfiles = dir([ baseFolder filesep centerFolders( indexCenter ).name filesep ]);
    VTAfiles(1:2) = [];
    for indexVTA = 1:numel( VTAfiles )
        fileNameVTA = [baseFolder filesep centerFolders(indexCenter).name filesep ...
            VTAfiles(indexVTA).name];
        reslice_nii(fileNameVTA,...
            'tmpVTAResliced.nii',heatmapAllCenters.voxsize,false)
        VTANifti = ea_load_nii( 'tmpVTAResliced.nii' );
        VTANifti.img( isnan( VTANifti.img )) = 0;
        VTANifti.img = round( VTANifti.img ); 
        [ xx,yy,zz ] = ind2sub( size( VTANifti.img ), find( VTANifti.img > 0 ));
        if ~isempty(xx)
            totalCounter = totalCounter + 1;
            VTAVoxelCoordinates = [ xx,yy,zz ];
            VTAWorldCoordinates = mapVoxelToWorld( VTAVoxelCoordinates,VTANifti );
            if contains( VTAfiles(indexVTA).name,'left' )
                VTAWorldCoordinates = ea_flip_lr_nonlinear( VTAWorldCoordinates );
            end
            aggregatedVTAsVoxelCoordinates = mapWorldToVoxel( VTAWorldCoordinates, heatmapAllCenters );
            indexLinear = sub2ind( size( heatmapAllCenters.img ),... 
                aggregatedVTAsVoxelCoordinates(:,1),...
                aggregatedVTAsVoxelCoordinates(:,2),...
                aggregatedVTAsVoxelCoordinates(:,3) );

            heatmapAllCenters.img( unique( indexLinear )) ...
                = heatmapAllCenters.img( unique( indexLinear )) ...
                + 1*reductionHeadacheFrequencyLastFollowUp( totalCounter, 1 )...
                /stimulationVoltageLastFollowUp( totalCounter, 1 );
            nImage.img( unique( indexLinear )) = ...
                    nImage.img( unique( indexLinear )) + 1; 
            if reductionHeadacheFrequencyLastFollowUp( totalCounter, 1 ) > .5 % responder
                nImageResponder.img( unique( indexLinear )) = ...
                    nImageResponder.img( unique(indexLinear) ) + 1;
                responderCounter = responderCounter + 1;
            else % non-responder, only n-image
                nImageNonResponder.img( unique( indexLinear )) = ...
                    nImageNonResponder.img( unique( indexLinear )) + 1;
                nonResponderCounter = nonResponderCounter + 1;
            end
        end
    end
end
delete tmpVTAResliced.nii
HEATMAPRAW = heatmapAllCenters;
NIMAGERAW = nImage;
toc
% no postprocessing at this point

%% Bootstrap
tic
doPlot = true;
totalCounterBootstrap = 0;
sweetSpotPercentiles = [ 0, 50, 75, 90 ];
overlapAnalysis = NaN( totalVTAs, numel( sweetSpotPercentiles ) + 1 );
fWait = waitbar( 0,'Bootstrap' );
for indexCenter = 1:numel(centerFolders)
    VTAfiles = dir([ baseFolder filesep centerFolders( indexCenter ).name filesep ]);
    VTAfiles(1:2) = [];
    for indexVTA = 1:numel( VTAfiles )
        waitbar( totalCounterBootstrap/totalVTAs, fWait, 'Bootstrap' );
        % reinit
        heatmapBootstrap = HEATMAPRAW;
        nImageBootstrap = NIMAGERAW;
        
        % get VTA and subtract it from maps
        fileNameVTA = [baseFolder filesep centerFolders(indexCenter).name filesep ...
            VTAfiles(indexVTA).name];
        reslice_nii(fileNameVTA,...
            'tmpVTAResliced.nii',heatmapBootstrap.voxsize,false)
        VTANifti = ea_load_nii( 'tmpVTAResliced.nii' );
        VTANifti.img( isnan( VTANifti.img )) = 0;
        VTANifti.img = round( VTANifti.img ); 
        [ xx,yy,zz ] = ind2sub( size( VTANifti.img ), find( VTANifti.img > 0 ));
        if ~isempty(xx)
            totalCounterBootstrap = totalCounterBootstrap + 1;
            VTAVoxelCoordinates = [ xx,yy,zz ];
            VTAWorldCoordinates = mapVoxelToWorld( VTAVoxelCoordinates, VTANifti );
            if contains( VTAfiles(indexVTA).name,'left' )
                VTAWorldCoordinates = ea_flip_lr_nonlinear( VTAWorldCoordinates );
            end
            aggregatedVTAsVoxelCoordinates = mapWorldToVoxel( VTAWorldCoordinates, heatmapBootstrap );
            indexLinear = sub2ind( size( heatmapBootstrap.img ),... 
                aggregatedVTAsVoxelCoordinates(:,1),...
                aggregatedVTAsVoxelCoordinates(:,2),...
                aggregatedVTAsVoxelCoordinates(:,3) );

            heatmapBootstrap.img( unique( indexLinear )) ...
                = heatmapBootstrap.img( unique( indexLinear )) ...
                - 1*reductionHeadacheFrequencyLastFollowUp( totalCounterBootstrap, 1 )...
                /stimulationVoltageLastFollowUp( totalCounterBootstrap, 1 );
            nImageBootstrap.img( unique( indexLinear )) = ...
                    nImageBootstrap.img( unique( indexLinear )) - 1; 
        end
        
        % postprocessing
        % discard voxels
        voxelsToDiscard = find(nImageBootstrap.img < 3);
        nImageBootstrap.img( voxelsToDiscard ) = 0;
        heatmapBootstrap.img( voxelsToDiscard ) = 0;
       
        % normalization
        voxelsToNormalize = find( nImageBootstrap.img > 0);
        heatmapBootstrap.img( voxelsToNormalize ) = ...
            heatmapBootstrap.img( voxelsToNormalize )...
            ./nImageBootstrap.img( voxelsToNormalize );
        
        overlapAnalysis( totalCounterBootstrap, 1) = ...
            reductionHeadacheFrequencyLastFollowUp( totalCounterBootstrap, 1 );
        for indexPercentile = 1:numel( sweetSpotPercentiles )
            mapSweetSpot = heatmapBootstrap;
            voxelsSignificant = find( mapSweetSpot.img > 0 );
            threshold = prctile( mapSweetSpot.img( voxelsSignificant ), ...
                sweetSpotPercentiles( indexPercentile ));
            voxelsNotSweetSpot = find( mapSweetSpot.img < threshold );
            voxelsSweetSpot = find( mapSweetSpot.img >= threshold );
            mapSweetSpot.img( voxelsNotSweetSpot ) = 0;
            mapSweetSpot.img( voxelsSweetSpot ) = 1;

            logicalVoxelsOverlap = ismember( indexLinear, ...
                voxelsSweetSpot );
            overlapAnalysis( totalCounterBootstrap, indexPercentile + 1 )...
                = sum( logicalVoxelsOverlap )...
                /sum( VTANifti.img(:) == 1 );
            
            if doPlot 
                tmpIdx = find( mapSweetSpot.img > 0);
                if ~isempty( tmpIdx )
                    hold on
                    [xx,yy,zz] = ind2sub( size( mapSweetSpot.img ), tmpIdx );
                    heatMapVoxelCoordinates = [xx,yy,zz];
                    heatMapWorldCoordinates = mapVoxelToWorld( heatMapVoxelCoordinates, mapSweetSpot );
                    markerSize = 20;
                    hMeanClinicalEffect = scatter3(heatMapWorldCoordinates(:,1),...
                        heatMapWorldCoordinates(:,2),...
                        heatMapWorldCoordinates(:,3),...
                        markerSize, [.5 .5 .5] ,'filled');
                end
            end
        end
    end
end
delete tmpVTAResliced.nii
close( fWait );
toc

%% Correlation/ modeling
correlationRho = zeros( numel( sweetSpotPercentiles ), 1 );
correlationPValue = zeros( numel( sweetSpotPercentiles ), 1 );

for k = 1:numel( sweetSpotPercentiles )
    [ correlationRho( k ), correlationPValue( k )] ...
        = corr( log( overlapAnalysis( :, k+1 ) * 100 ), overlapAnalysis( :,1 ), 'type', 'Spearman');
end
